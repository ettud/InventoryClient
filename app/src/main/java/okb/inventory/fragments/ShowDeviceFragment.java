package okb.inventory.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import okb.inventory.R;
import okb.inventory.adapters.ListDeviceAdapter;
import okb.inventory.fragments.presenters.ResultPresenter;
import okb.inventory.models.entities.ResponsiblePerson;

public class ShowDeviceFragment extends FragmentList {

    private ListView listPerson;
    private ResultPresenter presenter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        listPerson = view.findViewById(R.id.list);

        presenter = new ResultPresenter(this);
        presenter.showPersonDevice();

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
    }



    public void showPersonDevice(ResponsiblePerson person) {
        if(person != null) {
            ListDeviceAdapter adapter = new ListDeviceAdapter(getContext() , R.layout.list_item,
                    person.getInventory().getDevices());
            listPerson.setAdapter(adapter); 
        }
    }
}
