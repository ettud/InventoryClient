package okb.inventory.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import okb.inventory.R;
import okb.inventory.fragments.presenters.SuperFragmentPresenter;

public class FragmentList extends SuperFragment {

    protected ListView listView;
    protected SuperFragmentPresenter presenter;
    protected ArrayAdapter<String> adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_list, container, false);
        listView = view.findViewById(R.id.list);

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void showData(String[] data) {
        if(data == null || data.length == 0) return;

        adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1, data);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(itemClickListener);
    }

    protected AdapterView.OnItemClickListener itemClickListener;
}
