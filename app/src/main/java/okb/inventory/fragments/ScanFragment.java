package okb.inventory.fragments;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import okb.inventory.R;
import okb.inventory.fragments.presenters.ScanFragmentPresenter;
import okb.inventory.fragments.presenters.SuperFragmentPresenter;

public class ScanFragment extends SuperFragment {

    public static final int PERMISSION_CAMERA_CODE  = 1001;

    private SuperFragmentPresenter presenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.scan_fragment, container, false);
        presenter = new ScanFragmentPresenter(this);
        TextView buttonScan = view.findViewById(R.id.button_scan);
        buttonScan.setOnClickListener(v -> ((ScanFragmentPresenter)presenter).scanDevice());
        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PERMISSION_CAMERA_CODE:
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    ((ScanFragmentPresenter) presenter).scanDevice();
                }
                else showMessage(getString(R.string.not_camera_permissions));
                break;
        }
    }

    public void searchDevice(String serial) {
        ((ScanFragmentPresenter) presenter).searchDevice(serial);
    }
}
