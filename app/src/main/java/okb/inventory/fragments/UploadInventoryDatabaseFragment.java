package okb.inventory.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import okb.inventory.fragments.presenters.UploadInventoryDatabasePresenter;

public class UploadInventoryDatabaseFragment extends FragmentList {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        presenter = new UploadInventoryDatabasePresenter(this);
        ((UploadInventoryDatabasePresenter)presenter).loadBase();
        itemClickListener = (parent, view, position, id) -> {
            String name = adapter.getItem(position);
            ((UploadInventoryDatabasePresenter)presenter).uploadBase(name);
        };

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
        hideProgress();
        presenter.cancel();
    }

    @Override
    public void onStop() {
        super.onStop();
        hideProgress();
        presenter.cancel();
    }


}
