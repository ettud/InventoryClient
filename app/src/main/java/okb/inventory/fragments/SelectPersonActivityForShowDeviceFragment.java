package okb.inventory.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import okb.inventory.fragments.presenters.SelectPersonForShowDevicePresenter;

public class SelectPersonActivityForShowDeviceFragment extends FragmentList {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        presenter = new SelectPersonForShowDevicePresenter(this);
        ((SelectPersonForShowDevicePresenter) presenter).loadNames();
        itemClickListener = (parent, v, position, id) -> {
            String fullName = adapter.getItem(position);
            ((SelectPersonForShowDevicePresenter) presenter).loadPerson(fullName);
        };

        return view;
    }
}
