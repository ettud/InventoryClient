package okb.inventory.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import okb.inventory.fragments.presenters.SelectRoomPresenter;

public class SelectRoomFragment extends FragmentList {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = super.onCreateView(inflater, container, savedInstanceState);

        itemClickListener = (parent, v, position, id) -> {
            String number = adapter.getItem(position);
            ((SelectRoomPresenter) presenter).setCurrentRoom(number);
        };

        presenter = new SelectRoomPresenter(this);
        ((SelectRoomPresenter) presenter).loadRooms();

        return view;
    }
}
