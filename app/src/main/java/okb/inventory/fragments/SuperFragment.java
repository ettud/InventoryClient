package okb.inventory.fragments;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import okb.inventory.R;
import okb.inventory.fragments.presenters.SuperFragmentPresenter;

public class SuperFragment extends Fragment {

    protected SuperFragmentPresenter presenter;
    protected ProgressDialog progressDialog;

    public void showProgress(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(getContext(),
                    R.style.ProgressDialogStyle);

        progressDialog
                .getWindow()
                .setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        progressDialog.setOnCancelListener(dialog -> {
            if(presenter != null) {
                presenter.cancel();
            }
        });

        progressDialog.setMessage(message);
        progressDialog.show();

    }

    public void showMessageAction(String message, String action, View.OnClickListener listener) {
        Snackbar snackbar = Snackbar.make(getActivity().getCurrentFocus(),
                message, Snackbar.LENGTH_LONG);

        TextView textView = snackbar.getView()
                .findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);

        snackbar.setAction(action, listener);
        snackbar.show();
    }

    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog
                    .getWindow()
                    .clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    public void showMessage(String message) {
        Snackbar snackbar = Snackbar.make(getActivity().getCurrentFocus(),
                message, Snackbar.LENGTH_LONG);
        TextView textView = snackbar.getView()
                .findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    public void backStack() {
        getFragmentManager()
                .popBackStack();
    }

    @Override
    public void onStop() {
        if(presenter != null)
            presenter.cancel();
        super.onStop();
    }

    @Override
    public void onPause() {
        if(presenter != null)
            presenter.cancel();
        super.onPause();
    }
}
