package okb.inventory.fragments.presenters;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;

import com.google.zxing.integration.android.IntentIntegrator;
import com.orm.query.Condition;
import com.orm.query.Select;

import okb.inventory.R;
import okb.inventory.fragments.SuperFragment;
import okb.inventory.models.entities.Device;
import okb.inventory.models.entities.Inventory;
import okb.inventory.models.entities.ResponsiblePerson;
import okb.inventory.models.entities.Room;

public class ScanFragmentPresenter extends SuperFragmentPresenter {

    public static final int PERMISSION_CAMERA_CODE  = 1001;

    public ScanFragmentPresenter(SuperFragment fragment) {
        super(fragment);
    }

    public void searchDevice(String serial) {
        fragment.showProgress(fragment.getString(R.string.loading_data_please_wait));

        String currentPerson = settings.getCurrentResponsiblePerson();
        if(TextUtils.isEmpty(currentPerson)) {
            ResponsiblePerson person = Select
                    .from(ResponsiblePerson.class)
                    .first();

            if(person == null) {
                fragment.hideProgress();
                fragment.showMessage(fragment
                        .getString(R.string.error_not_selected_current_responsible_person));
                return;
            } else {
                currentPerson = person.toString();
                settings.setCurrentResponsiblePerson(currentPerson);
            }
        }

        String[] fullName = currentPerson.split(" ");
        if(fullName == null || fullName.length != 3) {
            fragment.hideProgress();
            fragment.showMessage(fragment.getString(R.string.error_incorrect_name));
            return;
        }

        ResponsiblePerson person = Select
                .from(ResponsiblePerson.class)
                .where(Condition.prop("surname").eq(fullName[0]))
                .where(Condition.prop("firstname").eq(fullName[1]))
                .where(Condition.prop("patronymic").eq(fullName[2]))
                .first();

        if(person == null) {
            fragment.hideProgress();
            fragment.showMessage(fragment.getString(R.string.database_is_empty));
            return;
        }

        Inventory inventory = Select
                .from(Inventory.class)
                .where(Condition.prop("person").eq(String.valueOf(person.getId())))
                .first();

        if(inventory == null) {
            fragment.hideProgress();
            fragment.showMessage(fragment.getString(R.string.database_is_empty));
            return;
        }

        Device device = Select
                .from(Device.class)
                .where(Condition.prop("serial").eq(serial))
                .where(Condition.prop("inventory").eq(String.valueOf(inventory.getId())))
                .first();

        if(device == null) {
            fragment.showMessage(fragment.getString(R.string.device_not_found));
            fragment.hideProgress();
            return;
        }

        String numberCurrentRoom = settings.getCurrentRoom();
        Room room =  Select
                .from(Room.class)
                .where(Condition.prop("number").eq(numberCurrentRoom))
                .first();

        if(room == null) {
            fragment.showMessage(fragment.getString(R.string.current_room_not_selected));
            return;
        }

        device.setRoom(room);

        if(device.isChecked()) {
            fragment.showMessage(fragment.getString(R.string.is_scanned));
            device.save();
        }else {
            device.setChecked(true);
            device.save();
            fragment.showMessage(fragment.getString(R.string.complete_device_is_scanned));
        }

        fragment.hideProgress();
    }

    public void scanDevice() {
        if(ContextCompat.checkSelfPermission(fragment.getContext(), Manifest.permission.CAMERA) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions( fragment.getActivity(),
                    new String[] {Manifest.permission.CAMERA}, PERMISSION_CAMERA_CODE);
            return;
        }

        String currentRoomNumber = settings.getCurrentRoom();
        if(TextUtils.isEmpty(currentRoomNumber)) {
            fragment.showMessage(fragment.getActivity()
                    .getString(R.string.current_room_not_selected));
            return;
        }

        IntentIntegrator scanIntegrator = new IntentIntegrator(fragment.getActivity());
        scanIntegrator.setOrientationLocked(false);
        scanIntegrator.initiateScan();
    }
}
