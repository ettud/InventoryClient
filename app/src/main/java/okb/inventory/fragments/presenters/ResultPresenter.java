package okb.inventory.fragments.presenters;

import okb.inventory.R;
import okb.inventory.fragments.ShowDeviceFragment;
import okb.inventory.fragments.SuperFragment;
import okb.inventory.models.database.FoundPersonCallback;
import okb.inventory.models.database.SearcherPerson;
import okb.inventory.models.entities.ResponsiblePerson;

public class ResultPresenter extends SuperFragmentPresenter {
    public ResultPresenter(SuperFragment fragment) {
        super(fragment);
    }

    private SearcherPerson searcherPerson;

    public void showPersonDevice() {
        fragment.showProgress(fragment.getString(R.string.loading_data_please_wait));
        String currentResponsiblePerson = settings.getCurrentResponsiblePerson();
        searcherPerson = new SearcherPerson();
        searcherPerson.search(currentResponsiblePerson, new FoundPersonCallback() {
            @Override
            public void onFound(ResponsiblePerson person) {
                ((ShowDeviceFragment) fragment).showPersonDevice(person);
                fragment.hideProgress();
            }

            @Override
            public void onFail() {
                fragment.showMessage(fragment.getString(R.string.database_is_empty));
                fragment.hideProgress();
            }
        });
    }

    @Override
    public void cancel() {
        if(searcherPerson != null) searcherPerson.cancel();
        super.cancel();
    }
}