package okb.inventory.fragments.presenters;

import android.text.TextUtils;

import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.List;

import okb.inventory.R;
import okb.inventory.fragments.SelectRoomFragment;
import okb.inventory.fragments.SuperFragment;
import okb.inventory.models.apiclient.database.download.DownloadRoomsCallback;
import okb.inventory.models.apiclient.database.download.LoaderRooms;
import okb.inventory.models.entities.Housing;
import okb.inventory.models.entities.Room;

public class SelectRoomPresenter extends SuperFragmentPresenter {
    public SelectRoomPresenter(SuperFragment fragment) {
        super(fragment);
    }

    LoaderRooms loader;
    public void loadRooms() {
        fragment.showProgress(fragment.getString(R.string.loading_data_please_wait));

        String currentHousing = settings.getCurrentHousing();
        if(TextUtils.isEmpty(currentHousing)) {
            fragment.hideProgress();
            fragment.showMessage(fragment.getString(R.string.housing_not_select));
            return;
        }

        Housing housing = (Housing) Select
                .from(Housing.class)
                .where(Condition.prop("title").eq(currentHousing))
                .first();

        if(housing == null) {
            fragment.hideProgress();
            fragment.showMessage(fragment.getString(R.string.housing_not_select));
            return;
        }

        List<Room> rooms = Select
                .from(Room.class)
                .where(Condition.prop("housing").eq(String.valueOf(housing.getId())))
                .list();

        if(rooms != null && rooms.size() != 0) {
            fragment.hideProgress();
            String[] namesRooms = new String[rooms.size()];
            for (int i = 0; i < rooms.size(); i++)
                namesRooms[i] = rooms.get(i).getNumber();
            ((SelectRoomFragment) fragment).showData(namesRooms);
            return;
        }

        String host = settings.getHost();
        String token = settings.getToken();

        loader = new LoaderRooms(host, token);
        loader.load(currentHousing, new DownloadRoomsCallback() {
            @Override
            public void onComplete(Room[] rooms) {
                fragment.hideProgress();
                for (Room room :
                        rooms) {
                    room.setHousing(housing);
                }
                Room.saveInTx(rooms);

                String[] namesRooms = new String[rooms.length];
                for (int i = 0; i < rooms.length; i++)
                    namesRooms[i] = rooms[i].getNumber();
                ((SelectRoomFragment) fragment).showData(namesRooms);
            }

            @Override
            public void onFail(int code, String error) {
                fragment.hideProgress();
                fragment.showMessage(fragment.getString(R.string.error_load_rooms));
            }

            @Override
            public void onFailConnectedServer(String error) {
                fragment.hideProgress();
                fragment.showMessage(fragment.getString(R.string.error_connection_server));
            }
        });
    }

    public void setCurrentRoom(String number) {
        settings.setCurrentRoom(number);
        fragment.showMessage(String.format("%s %s", number,
                fragment.getString(R.string.room_is_selected)));
        fragment.backStack();
    }

    @Override
    public void cancel() {
        if(loader != null) loader.cancel();
        super.cancel();
    }
}
