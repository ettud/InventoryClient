package okb.inventory.fragments.presenters;

import okb.inventory.fragments.SuperFragment;
import okb.inventory.models.Settings;

public class SuperFragmentPresenter {
    protected SuperFragment fragment;
    protected Settings settings;

    public SuperFragmentPresenter(SuperFragment fragment) {
        this.fragment = fragment;
        settings = new Settings(fragment.getContext());
    }


    public String getHost() {
        return settings.getHost();
    }

    public void logOut() {
        settings.deleteAccount();
        settings.clearDatabase();
    }

    public void cancel() {}
}
