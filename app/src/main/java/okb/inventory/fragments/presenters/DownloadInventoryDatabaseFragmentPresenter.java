package okb.inventory.fragments.presenters;

import okb.inventory.R;
import okb.inventory.fragments.DownloadInventoryDatabaseFragment;
import okb.inventory.fragments.SuperFragment;
import okb.inventory.models.apiclient.database.download.DatabaseLoader;
import okb.inventory.models.apiclient.database.download.DownloadDatabaseCallback;
import okb.inventory.models.apiclient.database.download.DownloadHousingsCallback;
import okb.inventory.models.apiclient.database.download.DownloadNamesCallback;
import okb.inventory.models.apiclient.database.download.LoadNames;
import okb.inventory.models.apiclient.database.download.LoaderHousings;
import okb.inventory.models.apiclient.database.download.LoaderRooms;
import okb.inventory.models.database.FoundPersonCallback;
import okb.inventory.models.database.SavePersonCallback;
import okb.inventory.models.database.SaverPerson;
import okb.inventory.models.database.SearcherPerson;
import okb.inventory.models.database.UpdatePersonCallback;
import okb.inventory.models.database.UpdaterPerson;
import okb.inventory.models.entities.Housing;
import okb.inventory.models.entities.ResponsiblePerson;

public class DownloadInventoryDatabaseFragmentPresenter extends SuperFragmentPresenter {
    public DownloadInventoryDatabaseFragmentPresenter(SuperFragment fragment) {
        super(fragment);
    }

    LoadNames namesLoader;

    public void loadNames() {
        String currentUsername = settings.getCurrentUsername();
        String host = settings.getHost();
        String token = settings.getToken();

        fragment.showProgress(fragment.getString(R.string.loading_data_please_wait));
        namesLoader = new LoadNames(host, token);
        namesLoader.load(currentUsername, new DownloadNamesCallback() {
            @Override
            public void onComplete(String[] names) {
                ((DownloadInventoryDatabaseFragment) fragment).showData(names);
                fragment.hideProgress();
            }

            @Override
            public void onFail(int code, String message) {
                fragment.hideProgress();
                fragment.showMessage(fragment.getString(R.string.error_load_database));
            }

            @Override
            public void onFailConnectionServer(String message) {
                fragment.hideProgress();
                fragment.showMessage(fragment.getString(R.string.error_connection_server));
            }
        });
    }


    @Override
    public void cancel() {
        if(namesLoader != null)
            namesLoader.cancel();

        if(loaderHousings != null)
            loaderHousings.cancel();

        if(loaderRooms != null)
            loaderRooms.cancel();

        if(databaseLoader != null)
            databaseLoader.cancel();

        super.cancel();
    }

    private UpdatePersonCallback updatePersonCallback = new UpdatePersonCallback() {
        @Override
        public void onComplete() {
            fragment.showMessage(fragment.getString(R.string.database_is_updated));
            fragment.hideProgress();
            fragment.backStack();
        }

        @Override
        public void onFail() {
            fragment.showMessage(fragment.getString(R.string.error_updated_database));
            fragment.hideProgress();
            fragment.backStack();
        }
    };

    private SavePersonCallback savePersonCallback = new SavePersonCallback() {
        @Override
        public void onComplete() {
            fragment.showMessage(fragment.getString(R.string.database_is_saved));
            fragment.hideProgress();
            fragment.backStack();
        }

        @Override
        public void onFail() {
            fragment.showMessage(fragment.getString(R.string.error_saved_database));
            fragment.hideProgress();
            fragment.backStack();
        }
    };

    LoaderHousings loaderHousings;
    LoaderRooms    loaderRooms;
    DatabaseLoader databaseLoader;

    public void loadBase(String name) {
        fragment.showProgress(fragment.getString(R.string.download_please_wait));
        String host  = settings.getHost();
        String token = settings.getToken();

        loaderHousings = new LoaderHousings(host, token);
        loaderHousings.load(new DownloadHousingsCallback() {

            @Override
            public void onComplete(Housing[] housings) {
                loaderRooms = new LoaderRooms(host, token);
                loaderHousings.load(new DownloadHousingsCallback() {

                    @Override
                    public void onComplete(Housing[] housings) {

                        databaseLoader = new DatabaseLoader(host, token);
                        databaseLoader.load(name, new DownloadDatabaseCallback() {

                            @Override
                            public void onComplete(ResponsiblePerson person) {

                                String currentPerson = settings.getCurrentResponsiblePerson();
                                if(currentPerson == null) {
                                    currentPerson = person.toString();
                                    settings.setCurrentResponsiblePerson(currentPerson);
                                }

                                SearcherPerson searcherPerson = new SearcherPerson();
                                searcherPerson.search(person.toString(), new FoundPersonCallback() {

                                    @Override
                                    public void onFound(ResponsiblePerson foundPerson) {
                                        UpdaterPerson updaterPerson = new UpdaterPerson();
                                        updaterPerson.update(foundPerson, person, updatePersonCallback);
                                    }

                                    @Override
                                    public void onFail() {
                                        SaverPerson saverPerson = new SaverPerson();
                                        saverPerson.save(person, savePersonCallback);
                                    }
                                });
                            }

                            @Override
                            public void onFail() {
                                fragment.hideProgress();
                                fragment.showMessage(fragment.getString(R.string.error_load_database));
                            }
                        });

                    }

                    @Override
                    public void onFail(int code, String message) {
                        fragment.hideProgress();
                        fragment.showMessage(fragment.getString(R.string.error_load_rooms));
                    }

                    @Override
                    public void onFailConnectionServer(String message) {
                        fragment.hideProgress();
                        fragment.showMessage(fragment.getString(R.string.error_connection_server));
                    }
                });
            }

            @Override
            public void onFail(int code, String message) {
                fragment.hideProgress();
                fragment.showMessage(fragment.getString(R.string.error_load_rooms));
            }

            @Override
            public void onFailConnectionServer(String message) {
                fragment.hideProgress();
                fragment.showMessage(fragment.getString(R.string.error_connection_server));
            }
        });
    }
}
