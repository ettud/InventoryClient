package okb.inventory.fragments.presenters;

import okb.inventory.R;
import okb.inventory.fragments.SettingsFragment;
import okb.inventory.fragments.SuperFragment;

public class SettingsPresenter extends SuperFragmentPresenter {


    public SettingsPresenter(SuperFragment fragment) {
        super(fragment);
    }

    public void saveUrl() {
        String url = ((SettingsFragment)fragment).getUrlHost();
        settings.saveHost(url);
    }

    public void clearDatabase() {
        settings.clearDatabase();
        fragment.showMessage(fragment.getString(R.string.database_is_cleared));
    }
}
