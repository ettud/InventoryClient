package okb.inventory.fragments.presenters;

import com.orm.SugarRecord;

import java.util.List;

import okb.inventory.R;
import okb.inventory.fragments.SelectHousingFragment;
import okb.inventory.fragments.SuperFragment;
import okb.inventory.models.apiclient.database.download.DownloadHousingsCallback;
import okb.inventory.models.apiclient.database.download.LoaderHousings;
import okb.inventory.models.entities.Housing;

public class SelectHousingPresenter extends SuperFragmentPresenter {

    public SelectHousingPresenter(SuperFragment fragment) {
        super(fragment);
    }

    LoaderHousings loaderHousings;
    public void loadHousings() {
        fragment.showProgress(fragment.getString(R.string.loading_data_please_wait));
        List<Housing> housings = SugarRecord.listAll(Housing.class);

        if (housings.size() == 0) {

            String host = settings.getHost();
            String token = settings.getToken();
            loaderHousings = new LoaderHousings(host, token);
            loaderHousings.load(new DownloadHousingsCallback() {
                @Override
                public void onComplete(Housing[] housings) {
                    fragment.hideProgress();
                    SugarRecord.saveInTx(housings);

                    String[] namesHousings = new String[housings.length];

                    for (int i = 0; i < housings.length; i++) {
                        namesHousings[i] = housings[i].getTitle();
                    }

                    ((SelectHousingFragment) fragment).showData(namesHousings);
                }

                @Override
                public void onFail(int code, String error) {
                    fragment.hideProgress();
                    fragment.showMessage(fragment.getString(R.string.error_load_housings));
                }

                @Override
                public void onFailConnectionServer(String error) {
                    fragment.hideProgress();
                    fragment.showMessage(fragment.getString(R.string.error_connection_server));
                }
            });
        } else {
            String[] nameHousings = new String[housings.size()];
            for (int i = 0; i < nameHousings.length; i++)
                nameHousings[i] = housings.get(i).getTitle();

            ((SelectHousingFragment) fragment).showData(nameHousings);
            fragment.hideProgress();
            return;
        }
    }

    public void selectHousing(String title) {
        settings.setCurrentHousing(title);
        fragment.showMessage(String.format("%s %s",
                title, fragment.getString(R.string.housing_is_selected)));
        fragment.backStack();
    }

    @Override
    public void cancel() {
        if(loaderHousings != null)
            loaderHousings.cancel();
        super.cancel();
    }
}
