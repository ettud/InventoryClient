package okb.inventory.fragments.presenters;

import okb.inventory.R;
import okb.inventory.fragments.SuperFragment;
import okb.inventory.fragments.UploadInventoryDatabaseFragment;
import okb.inventory.models.apiclient.database.upload.DatabaseUploader;
import okb.inventory.models.apiclient.database.upload.UploadBaseCallback;
import okb.inventory.models.database.FoundPersonCallback;
import okb.inventory.models.database.LoadNamesCallback;
import okb.inventory.models.database.LoaderNamesFromDatabase;
import okb.inventory.models.database.SearcherPerson;
import okb.inventory.models.entities.ResponsiblePerson;

public class UploadInventoryDatabasePresenter extends SuperFragmentPresenter {


    public UploadInventoryDatabasePresenter(SuperFragment fragment) {
        super(fragment);
    }

    LoaderNamesFromDatabase loaderNames;
    public void loadBase() {
        loaderNames = new LoaderNamesFromDatabase();
        loaderNames.load(new LoadNamesCallback() {
            @Override
            public void onComplete(String[] names) {
                ((UploadInventoryDatabaseFragment)fragment).showData(names);
            }

            @Override
            public void onFail() {
                fragment.showMessage(fragment.getString(R.string.database_is_empty));
            }
        });
    }

    DatabaseUploader uploader;
    SearcherPerson searcherPerson;
    public void uploadBase(String name) {
        fragment.showProgress(fragment.getString(R.string.data_uploaded_plase_wait));
        searcherPerson = new SearcherPerson();
        searcherPerson.search(name, new FoundPersonCallback() {
            @Override
            public void onFound(ResponsiblePerson person) {
                String host = settings.getHost();
                String token = settings.getToken();

                uploader = new DatabaseUploader();

                uploader.upload(host, token, person, new UploadBaseCallback() {
                    @Override
                    public void onComplete() {
                        fragment.hideProgress();
                        fragment.showMessage(fragment.getString(R.string.database_is_uploaded));
                    }

                    @Override
                    public void onFail(int code, String message) {
                        fragment.hideProgress();
                        fragment.showMessage(fragment.getString(R.string.database_is_not_uploaded));
                    }

                    @Override
                    public void onFailRequest(String message) {
                        fragment.hideProgress();
                        fragment.showMessage(fragment.getString(R.string.database_is_not_uploaded));
                    }
                });
            }

            @Override
            public void onFail() {
                fragment.showMessage("ОШИБКАААААААААААААААААА");
            }
        });
    }

    @Override
    public void cancel() {
        if(loaderNames != null)
            loaderNames.cancel();

        if(searcherPerson != null)
            searcherPerson.cancel();

        if(uploader != null)
            uploader.cancel();

        super.cancel();
    }
}
