package okb.inventory.fragments.presenters;

import okb.inventory.R;
import okb.inventory.fragments.SelectCurrentResponsiblePersonFragment;
import okb.inventory.activities.SuperActivity;
import okb.inventory.fragments.SuperFragment;
import okb.inventory.models.database.LoadNamesCallback;
import okb.inventory.models.database.LoaderNamesFromDatabase;
import okb.inventory.presenters.SuperPresenter;

public class SelectCurrentResponsiblePersonPresenter extends SuperFragmentPresenter {

    public SelectCurrentResponsiblePersonPresenter(SuperFragment fragment) {
        super(fragment);
    }

    public void saveCurrentPerson(String fullName) {
        settings.setCurrentResponsiblePerson(fullName);
        fragment.showMessage(String.format("%s %s", fullName,
                fragment.getString(R.string.is_selected)));
        fragment.backStack();
    }

    private LoaderNamesFromDatabase loader;
    public void loadBase() {
        loader = new LoaderNamesFromDatabase();
        loader.load(new LoadNamesCallback() {
            @Override
            public void onComplete(String[] names) {
                ((SelectCurrentResponsiblePersonFragment) fragment).showData(names);
            }

            @Override
            public void onFail() {
                fragment.showMessage(fragment.getString(R.string.database_is_empty));
            }
        });
    }

    @Override
    public void cancel() {
        if(loader != null)
            loader.cancel();
        super.cancel();
    }
}
