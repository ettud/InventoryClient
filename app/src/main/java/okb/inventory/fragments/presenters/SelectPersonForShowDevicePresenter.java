package okb.inventory.fragments.presenters;

import android.os.Bundle;

import okb.inventory.R;
import okb.inventory.fragments.SelectPersonActivityForShowDeviceFragment;
import okb.inventory.fragments.ShowDeviceFragment;
import okb.inventory.fragments.SuperFragment;
import okb.inventory.models.database.FoundPersonCallback;
import okb.inventory.models.database.LoadNamesCallback;
import okb.inventory.models.database.LoaderNamesFromDatabase;
import okb.inventory.models.database.SearcherPerson;
import okb.inventory.models.entities.ResponsiblePerson;

public class SelectPersonForShowDevicePresenter extends SuperFragmentPresenter {
    public SelectPersonForShowDevicePresenter(SuperFragment fragment) {
        super(fragment);
    }

    LoaderNamesFromDatabase loaderNamesFromDatabase;

    public void loadNames() {
        fragment.showProgress(fragment.getString(R.string.loading_data_please_wait));
        loaderNamesFromDatabase = new LoaderNamesFromDatabase();
        loaderNamesFromDatabase.load(new LoadNamesCallback() {
            @Override
            public void onComplete(String[] names) {
                fragment.hideProgress();
                ((SelectPersonActivityForShowDeviceFragment) fragment).showData(names);
            }

            @Override
            public void onFail() {
                fragment.hideProgress();
                fragment.showMessage(fragment.getString(R.string.database_is_empty));
            }
        });
    }

    SearcherPerson searcherPerson;
    public void loadPerson(String fullName) {
        fragment.showProgress(fragment.getString(R.string.loading_data_please_wait));
        searcherPerson = new SearcherPerson();
        searcherPerson.search(fullName, new FoundPersonCallback() {
            @Override
            public void onFound(ResponsiblePerson person) {
                fragment.hideProgress();

                Bundle args = new Bundle();
                args.putSerializable(ResponsiblePerson.TAG, person);

                ShowDeviceFragment deviceFragment = new ShowDeviceFragment();
                deviceFragment
                        .setArguments(args);

                fragment
                        .getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragmentContainer, deviceFragment)
                        .addToBackStack(null)
                        .commit();
            }

            @Override
            public void onFail() {
                fragment.hideProgress();
                fragment.showMessage(fragment.getString(R.string.database_is_empty));
            }
        });
    }

    @Override
    public void cancel() {
        if(loaderNamesFromDatabase != null)
            loaderNamesFromDatabase.cancel();

        if(searcherPerson != null)
            searcherPerson.cancel();

        super.cancel();
    }
}
