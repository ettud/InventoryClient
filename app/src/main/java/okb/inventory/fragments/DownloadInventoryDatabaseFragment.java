package okb.inventory.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import okb.inventory.fragments.presenters.DownloadInventoryDatabaseFragmentPresenter;

public class DownloadInventoryDatabaseFragment extends FragmentList {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        presenter = new DownloadInventoryDatabaseFragmentPresenter(this);

        itemClickListener = (parent, v, position, id) -> {
            String name = adapter.getItem(position);
            ((DownloadInventoryDatabaseFragmentPresenter) presenter).loadBase(name);
        };

        ((DownloadInventoryDatabaseFragmentPresenter) presenter).loadNames();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.cancel();
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.cancel();
    }
}

