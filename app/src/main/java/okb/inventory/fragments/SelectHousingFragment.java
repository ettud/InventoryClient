package okb.inventory.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import okb.inventory.fragments.presenters.SelectHousingPresenter;

public class SelectHousingFragment extends FragmentList {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = super.onCreateView(inflater, container, savedInstanceState);

        itemClickListener = (parent, v, position, id) -> {
            String titleHousing = adapter.getItem(position);
            ((SelectHousingPresenter) presenter).selectHousing(titleHousing);
        };

        presenter = new SelectHousingPresenter(this);
        ((SelectHousingPresenter) presenter).loadHousings();

        return view;
    }
}
