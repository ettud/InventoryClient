package okb.inventory.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Objects;

import okb.inventory.R;
import okb.inventory.activities.LoginActivity;
import okb.inventory.activities.MainActivity;
import okb.inventory.activities.SuperActivity;
import okb.inventory.fragments.presenters.SettingsPresenter;

import static android.view.View.*;

public class SettingsFragment extends SuperFragment {

    private EditText urlEditor;
    private TextView btnSave;
    private TextView btnClearDatabase;
    private TextView btnLogOut;

    SettingsPresenter presenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        urlEditor = view.findViewById(R.id.url_editor);
        btnSave = view.findViewById(R.id.btn_save);
        btnClearDatabase = view.findViewById(R.id.btnClearDatabase);
        btnLogOut = view.findViewById(R.id.btnLogOut);

        btnSave.setOnClickListener(buttonSaveSettingsListener);
        btnClearDatabase.setOnClickListener(buttonClearDatabaseListener);
        btnLogOut.setOnClickListener(buttonLogOutListener);

        presenter = new SettingsPresenter(this);

        urlEditor.setText(presenter.getHost());

        return view;
    }

    public String getUrlHost() {
        return urlEditor.getText().toString();
    }

    OnClickListener buttonSaveSettingsListener = v -> {
        presenter.saveUrl();
        getFragmentManager().popBackStack();
    };

    OnClickListener buttonClearDatabaseListener = v -> presenter.clearDatabase();

    OnClickListener buttonLogOutListener = v -> {
        presenter.logOut();
        Objects.requireNonNull(getActivity()).startActivity(new Intent(getActivity(), LoginActivity.class));
    };

    @Override
    public void onStop() {
        super.onStop();
        presenter.saveUrl();
    }
}
