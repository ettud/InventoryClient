package okb.inventory.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import okb.inventory.R;
import okb.inventory.models.entities.Device;

public class ListDeviceAdapter extends ArrayAdapter<Device> {

    private List<Device> devices;
    private LayoutInflater inflater;
    private Context context;
    private int layout;

    public ListDeviceAdapter(@NonNull Context context, int resource, @NonNull List<Device> devices) {
        super(context, resource, devices);
        this.devices  = devices;
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.layout = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = inflater.inflate(layout, parent, false);

        TextView nameDevice    = view.findViewById(R.id.name_device);
        TextView serialDevice  = view.findViewById(R.id.serial_device);
        TextView location      = view.findViewById(R.id.location);
        ImageView statusDevice = view.findViewById(R.id.status_device);

        Device device = devices.get(position);

        nameDevice.setText(device.getModel());
        serialDevice.setText(device.getSerial());

        if(device.getRoom() != null) {
            String locationString = null;
            if(device.getRoom().getHousing() != null)
                locationString = String.format("%s: %s, %s: %s",
                        context.getString(R.string.housing), device.getRoom().getHousing().getTitle(),
                        context.getString(R.string.room), device.getRoom().getNumber());
            else
                locationString = device.getRoom().getNumber();
            location.setText(locationString);
        }

        int idImage;
        Resources resources = context.getResources();

        if(device.getSerial().equals("2018.03.2.1910")) {
            int a = 1;
        }

        if(device.isChecked()){
            idImage = resources.getIdentifier("ic_check" ,"drawable", context.getPackageName());
        } else  {
            idImage = resources.getIdentifier("ic_close" ,"drawable", context.getPackageName());
        }
        statusDevice.setImageResource(idImage);

        return view;
    }

    @Override
    public int getCount() {
        return devices.size();
    }
}
