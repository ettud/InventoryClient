package okb.inventory.presenters;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okb.inventory.R;
import okb.inventory.activities.MainActivity;
import okb.inventory.activities.SuperActivity;
import okb.inventory.fragments.DownloadInventoryDatabaseFragment;
import okb.inventory.fragments.ScanFragment;
import okb.inventory.fragments.SelectCurrentResponsiblePersonFragment;
import okb.inventory.fragments.SelectHousingFragment;
import okb.inventory.fragments.SelectPersonActivityForShowDeviceFragment;
import okb.inventory.fragments.SelectRoomFragment;
import okb.inventory.fragments.SettingsFragment;
import okb.inventory.fragments.SuperFragment;
import okb.inventory.fragments.UploadInventoryDatabaseFragment;

public class MainPresenter extends SuperPresenter {

    public static final int PERMISSION_STORAGE_CODE = 1002;

    public MainPresenter(SuperActivity activity) {
        super(activity);
    }

    public void loadDatabase() {
        if(ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity,
                    new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_STORAGE_CODE);
            return;
        }

        redirectToLoadDatabaseActivity();
    }

    public String getSerial(String string) {
        Pattern pattern = Pattern.compile("Inv=(.+)\\|");
        Matcher matcher = pattern.matcher(string);
        return matcher.find() ? matcher.group(1) : null;
    }

    public void redirectToMain() {
        setFragment(new ScanFragment());
    }

    public void redirectToDeviceActivity() {
        setFragment(new SelectPersonActivityForShowDeviceFragment());
    }

    public void redirectToSettingsActivity() {
        setFragment(new SettingsFragment());
    }

    public void redirectToLoadDatabaseActivity() {
        setFragment(new DownloadInventoryDatabaseFragment());
    }

    public void redirectToSelectResponsiblePersonActivity() {
        setFragment(new SelectCurrentResponsiblePersonFragment());
    }

    public void redirectToUploadDatabaseActivity() {
        setFragment(new UploadInventoryDatabaseFragment());
    }

    public void redirectToHousingsSettings() {
        setFragment(new SelectHousingFragment());
    }

    public void redirectToRoomsSettings() {
        setFragment(new SelectRoomFragment());
    }

    private void setFragment(SuperFragment fragment) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.fragmentContainer, fragment)
                .addToBackStack(null)
                .commit();
    }

    public void showUsername(TextView peronNameView) {
        String username = settings.getCurrentUsername();
        ((MainActivity) activity).showPersonName(peronNameView, username);
    }
}
