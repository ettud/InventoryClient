package okb.inventory.presenters;

import android.content.Intent;

import okb.inventory.activities.LoginActivity;
import okb.inventory.activities.MainActivity;
import okb.inventory.activities.SuperActivity;
import okb.inventory.models.Settings;

public class SuperPresenter {
    protected SuperActivity activity;
    protected Settings settings;

    public SuperPresenter(SuperActivity activity) {
        this.activity = activity;
        settings = new Settings(activity);
    }

    public void logOut() {
        settings.deleteAccount();
        settings.clearDatabase();
    }

    public String getHost() {
        return settings.getHost();
    }

    public void redirectToLoginActivity() {
        activity.startActivity(new Intent(activity,
                LoginActivity.class));
        activity.finish();
    }

    public void redirectToMainActivity() {
        activity.startActivity(new Intent(activity,
                MainActivity.class));
        activity.finish();
    }
}
