package okb.inventory.presenters;

import android.content.Intent;

import okb.inventory.R;
import okb.inventory.activities.MainActivity;
import okb.inventory.activities.SuperActivity;
import okb.inventory.models.apiclient.ApiClient;
import okb.inventory.models.apiclient.authentication.Authenticator;
import okb.inventory.models.apiclient.authentication.LoginCallback;
import okb.inventory.models.Settings;
import okb.inventory.models.Validator;
import okb.inventory.activities.LoginActivity;

public class LoginPresenter extends SuperPresenter {

    public LoginPresenter(SuperActivity activity) {
        super(activity);
    }

    public void reLogin() {
        boolean isLogin = settings.isLogin();
        if(isLogin) redirectMainActivity();
    }

    private void redirectMainActivity() {
        activity.startActivity(new Intent(activity, MainActivity.class));
        activity.finish();
    }

    public void authenticate(String login, String password) {
        activity.showProgress(activity.getString(R.string.download_please_wait));
        Settings settings = new Settings(activity);
        String host = settings.getHost();
        Authenticator.Authenticate(host, login, password, new LoginCallback() {
            @Override
            public void onComplete(String username, String token) {
                settings.saveAccount(login, password, username, token, true);
                activity.hideProgress();
                redirectMainActivity();
            }

            @Override
            public void onFail(int code, String message) {
                activity.hideProgress();
                if(code == ApiClient.BAD_REQUEST_CODE) {
                    activity.showMessage(activity.getString(R.string.incorrect_user_data));
                    settings.deleteAccount();
                    return;
                }
                else {
                    activity.showMessage(String.format(
                            "%s: %d %s", activity.getString(R.string.error_authenticate), code,
                            message
                    ));
                }
            }

            @Override
            public void onFailRequest(String message) {
                activity.hideProgress();
                activity.showMessage(String.format("%s: %s",
                        activity.getString(R.string.error_connection_server), message));
            }
        });
    }

    public boolean validate(String login, String password) {
        if(!Validator.validateLogin(login)) {
            ((LoginActivity)activity).setLoginError(activity.getString(R.string.incorrect_login));
            return false;
        }

        if(!Validator.validatePassword(password)) {
            ((LoginActivity)activity).setPasswordError(activity.getString(R.string.incorrect_password));
            return false;
        }

        return true;
    }
}
