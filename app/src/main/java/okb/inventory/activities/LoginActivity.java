package okb.inventory.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import okb.inventory.R;
import okb.inventory.presenters.LoginPresenter;

public class LoginActivity extends SuperActivity {

    private final int PASSWORD_MIN_LENGTH = 4;

    private TextView loginView;
    private TextView passwordView;
    private TextView signInButton;

    private LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new LoginPresenter(this);
        presenter.reLogin();

        setContentView(R.layout.activity_login);
        initialViews();

        getSupportActionBar().setTitle(getString(R.string.authorize));
    }

    public void setLoginError(String error) {
        loginView.setError(error);
        loginView.requestFocus();
    }

    public void setPasswordError(String error) {
        passwordView.setError(error);
        passwordView.requestFocus();
    }

    private void initialViews() {
        loginView    = findViewById(R.id.loginView);
        passwordView = findViewById(R.id.passwordView);
        signInButton = findViewById(R.id.signInButton);
        signInButton.setOnClickListener(signInButtonListener);
    }

    private View.OnClickListener signInButtonListener = v -> {
        String login = loginView.getText().toString();
        String password = passwordView.getText().toString();
        if(presenter.validate(login, password)) {
            presenter.authenticate(login, password);
            dismissKeyboard();
        }
    };
}
