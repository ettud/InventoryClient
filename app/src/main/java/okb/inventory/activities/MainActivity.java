package okb.inventory.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import okb.inventory.R;
import okb.inventory.fragments.ScanFragment;
import okb.inventory.presenters.MainPresenter;

public class MainActivity extends SuperActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private final String TAG = "Main Activity";

    MainPresenter presenter;
    DrawerLayout drawer;

    ScanFragment scanFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                if(slideOffset == 0) {
                    return;
                }

                TextView peronNameView = drawerView.findViewById(R.id.name_person);
                presenter.showUsername(peronNameView);
            }
        };

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        presenter = new MainPresenter(this);

        scanFragment = new ScanFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragmentContainer,scanFragment)
                .commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_drawer, menu);
        return true;
    }

//    settings in action bar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.housing_settings:
                presenter.redirectToHousingsSettings();
                break;

            case R.id.room_settings:
                presenter.redirectToRoomsSettings();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_main:
                presenter.redirectToMain();
                break;
            case R.id.nav_result:
                presenter.redirectToDeviceActivity();
                break;

            case R.id.nav_load_database:
                presenter.redirectToLoadDatabaseActivity();
                break;

            case R.id.nav_unload_database:
                presenter.redirectToUploadDatabaseActivity();
                break;

            case R.id.select_current_user:
                presenter.redirectToSelectResponsiblePersonActivity();
                break;

            case R.id.nav_settings:
                presenter.redirectToSettingsActivity();
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case MainPresenter.PERMISSION_STORAGE_CODE:
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    presenter.loadDatabase();
                }
                else showMessage(getString(R.string.needed_permission_storage));
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == 0 || data == null) return;

        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(scanResult != null) {
            String qrCode   = scanResult.getContents();
            String serial = presenter.getSerial(qrCode);

            if(serial == null)  {
                showMessage(getString(R.string.fail_scanned_code));
                return;
            }

            scanFragment.searchDevice(serial);
        } else {
            showMessage(getString(R.string.fail_scanned_code));
            Log.i(TAG, getString(R.string.fail_scanned_code));
        }
    }

    public void showPersonName(TextView peronNameView, String fullName) {
        peronNameView.setText(fullName);
    }
}
