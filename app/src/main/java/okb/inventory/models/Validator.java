package okb.inventory.models;

import android.text.TextUtils;

public class Validator {
    public static final int PASSWORD_MIN_LENGTH = 4;
    public static final int LOGIN_MIN_LENGTH    = 4;

    public static boolean validateLogin(String login) {
        return login != null && !TextUtils.isEmpty(login) && login.length() >= LOGIN_MIN_LENGTH;
    }

    public static boolean validatePassword(String password) {
        return password != null && !TextUtils.isEmpty(password) && password.length() >= PASSWORD_MIN_LENGTH;
    }
}
