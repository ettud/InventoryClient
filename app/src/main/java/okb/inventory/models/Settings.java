package okb.inventory.models;

import android.content.Context;
import android.content.SharedPreferences;

import com.orm.SugarDb;
import com.orm.SugarRecord;

import okb.inventory.models.entities.Device;
import okb.inventory.models.entities.Housing;
import okb.inventory.models.entities.Inventory;
import okb.inventory.models.entities.ResponsiblePerson;
import okb.inventory.models.entities.Room;

public class Settings {

    private static final String CONNECTION_SETTINGS        = "CONNECTION_SETTINGS";
    private static final String DEFAULT_DOMAIN             = "http://10.250.2.38";
    private static final String KEY_DOMAIN                 = "DOMAIN";

    private static final String ACCOUNT_SETTINGS           = "ACCOUNT_SETTINGS";
    private static final String TOKEN                      = "TOKEN";
    private static final String CURRENT_USER               = "CURRENT_USER";

    private static final String IS_LOGIN                   = "IS_LOGIN";
    private static final String LOGIN                      = "LOGIN";
    private static final String PASSWORD                   = "PASSWORD";

    private static final String DATABASE_SETTINGS          = "DATABASE_SETTINGS";
    private static final String CURRENT_RESPONSIBLE_PERSON = "CURRENT_RESPONSIBLE_PERSON";
    private static final String CURRENT_HOUSING            = "CURRENT_HOUSING";
    private static final String CURRENT_ROOM               = "CURRENT_ROOM";

    private static final String  DEFAULT_STRING_VALUE      = null;
    private static final boolean DEFAULT_BOOL_VALUE        = false;


    private Context context;
    private SharedPreferences connectionSettings;
    private SharedPreferences accountSettings;
    private SharedPreferences databaseSettings;

    public Settings(Context context) {
        this.context       = context;
        connectionSettings = context.getSharedPreferences(CONNECTION_SETTINGS, Context.MODE_PRIVATE);
        accountSettings    = context.getSharedPreferences(ACCOUNT_SETTINGS,    Context.MODE_PRIVATE);
        databaseSettings   = context.getSharedPreferences(DATABASE_SETTINGS,   Context.MODE_PRIVATE);
    }

    public void saveHost(String url) {
        connectionSettings.edit()
                .putString(KEY_DOMAIN, url)
                .apply();
    }

    public String getHost() {
        String url = connectionSettings.getString(KEY_DOMAIN, DEFAULT_DOMAIN);
        return !url.contains("http://") ? String.format("http://%s", url) : url;
    }

    public String getToken() {
        return accountSettings.getString(TOKEN, DEFAULT_STRING_VALUE);
    }

    public String getCurrentUsername() {
        return accountSettings.getString(CURRENT_USER, DEFAULT_STRING_VALUE);
    }

    public String getLogin() {
        return accountSettings.getString(LOGIN, DEFAULT_STRING_VALUE);
    }

    public String getPassword() {
        return accountSettings.getString(PASSWORD, DEFAULT_STRING_VALUE);
    }

    public boolean isLogin() {
        return accountSettings.getBoolean(IS_LOGIN, DEFAULT_BOOL_VALUE);
    }

    public void deleteAccount() {
        accountSettings.edit().clear().apply();
    }

    public void saveAccount(String login, String password, String username, String token,
                                 boolean isAuthenticate) {
        accountSettings.edit()
                .putString(LOGIN, login)
                .putString(PASSWORD, password)
                .putString(TOKEN, token)
                .putBoolean(IS_LOGIN, isAuthenticate)
                .putString(CURRENT_USER, username)
                .apply();
    }

    public void clearDatabase() {
        SugarRecord.deleteAll(ResponsiblePerson.class);
        SugarRecord.deleteAll(Inventory.class);
        SugarRecord.deleteAll(Device.class);
        SugarRecord.deleteAll(Room.class);


        setCurrentRoom(null);
        setCurrentHousing(null);
        setCurrentResponsiblePerson(null);
    }

    public void setCurrentResponsiblePerson(String name) {
        databaseSettings
                .edit()
                .putString(CURRENT_RESPONSIBLE_PERSON, name)
                .apply();
    }

    public String getCurrentResponsiblePerson() {
        return databaseSettings.getString(CURRENT_RESPONSIBLE_PERSON, DEFAULT_STRING_VALUE);
    }

    public void setCurrentHousing(String title) {
        databaseSettings
                .edit()
                .putString(CURRENT_HOUSING, title)
                .apply();
    }

    public String getCurrentHousing() {
        return databaseSettings.getString(CURRENT_HOUSING, DEFAULT_STRING_VALUE);
    }

    public void setCurrentRoom(String title) {
        databaseSettings
                .edit()
                .putString(CURRENT_ROOM, title)
                .apply();
    }

    public String getCurrentRoom() {
        return databaseSettings.getString(CURRENT_ROOM, DEFAULT_STRING_VALUE);
    }
}
