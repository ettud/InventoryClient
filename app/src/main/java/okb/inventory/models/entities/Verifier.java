package okb.inventory.models.entities;

import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.List;

public class Verifier extends SugarRecord implements IEntity {
    private String surname;
    private String firstname;
    private String patronymic;
    private List<ResponsiblePerson> responsiblePeoples;

    public String getSurname() {
        return surname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public List<ResponsiblePerson> getResponsiblePeoples() {
        return responsiblePeoples;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }
}
