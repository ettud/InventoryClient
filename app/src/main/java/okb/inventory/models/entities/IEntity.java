package okb.inventory.models.entities;

import java.io.Serializable;

public interface IEntity extends Serializable {
    String emptyString = "";
    boolean isEmpty();
}
