package okb.inventory.models.entities;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.util.List;

public class Inventory extends SugarRecord<Inventory> implements IEntity {
    @Ignore
    private List<Device> devices;
    private String dateLoad;

    private ResponsiblePerson person;

    public void setPerson(ResponsiblePerson person) {
        this.person = person;
    }

    public Inventory() {}

    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }

    public void setDateLoad(String dateLoad) {
        this.dateLoad = dateLoad;
    }

    public List<Device> getDevices() {
        return devices;
    }

    public String getDateLoad() {
        return dateLoad;
    }

    @Override
    public boolean isEmpty() {
        if(dateLoad == null || dateLoad.equals(emptyString)) return true;

        for (Device device :
                devices) {
            if(device.isEmpty()) return true;
        }

        return false;
    }
}
