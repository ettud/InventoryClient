package okb.inventory.models.entities;

import android.text.TextUtils;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.io.Serializable;

public class Device extends SugarRecord<Device> implements IEntity {
    private String    model;
    private String    serial;
    private Room      room;
    private boolean   isChecked;
    private Inventory inventory;

    public Device() {}

    public void setChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }

    public String getModel() {
        return model;
    }

    public String getSerial() {
        return serial;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Room getRoom() {
        return room;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public Inventory getInventory() {
        return inventory;
    }
    @Override
    public boolean isEmpty() {
        return TextUtils.isEmpty(model) || TextUtils.isEmpty(serial);
    }
}
