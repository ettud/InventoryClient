package okb.inventory.models.entities;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.util.List;

public class Housing extends SugarRecord implements IEntity {

    private String     title;
    @Ignore
    private List<Room> rooms;

    public String getTitle() {
        return title;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }
}
