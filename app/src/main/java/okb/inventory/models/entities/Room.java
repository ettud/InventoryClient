package okb.inventory.models.entities;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.util.List;

public class Room extends SugarRecord<Room> implements IEntity {
    private String       number;
    private Housing housing;
    @Ignore
    private List<Device> devices;

    public String getNumber() {
        return number;
    }

    public Housing getHousing() {
        return housing;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    public void setHousing(Housing housing) {
        this.housing = housing;
    }
}
