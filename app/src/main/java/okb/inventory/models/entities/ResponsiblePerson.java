package okb.inventory.models.entities;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.io.Serializable;

public class ResponsiblePerson extends SugarRecord<ResponsiblePerson> implements IEntity {
    private String surname;
    private String firstname;
    private String patronymic;
    private Inventory inventory;

    public static final String TAG = "RESPONSIBLE_PERSON";

    public ResponsiblePerson() {}

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getSurname() {
        return surname;
    }

    public String getFirstname() {
        return firstname;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s", surname, firstname, patronymic);
    }

    @Override
    public boolean isEmpty() {
        return surname == null || firstname == null || patronymic == null ||
                surname.equals(emptyString) || firstname.equals(emptyString) ||
                patronymic.equals(emptyString);
    }
}
