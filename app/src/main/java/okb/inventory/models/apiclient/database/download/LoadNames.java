package okb.inventory.models.apiclient.database.download;

public class LoadNames {

    private String host;
    private String token;
    DownloadNamesTask task;

    public LoadNames(String host, String token) {
        this.host = host;
        this.token = token;
    }

    public void load(String username, DownloadNamesCallback callback) {
        task = new DownloadNamesTask(host, token, username, callback);
        task.execute();
    }

    public void cancel() {
        if(task != null && !task.isCancelled())
            task.cancel(true);
    }
}
