package okb.inventory.models.apiclient;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.OkHttpClient.Builder;

public class ApiClient {

    public static final int OK_CODE          = 200;
    public static final int BAD_REQUEST_CODE = 400;

    private static final MediaType JSON       = MediaType.parse("application/json;");
    private static final String AUTHORIZATION = "Authorization";
    private static final int TIMEOUT          = 30;

    private OkHttpClient client;
    public ApiClient() {
        client = new Builder()
                .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .build();
    }

    public Response postRequest(String url, String data) throws IOException {

        RequestBody body = RequestBody
                .create(JSON, data);

        Request request = new Request.Builder()
                .post(body)
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        return response;
    }

    public Response postRequest(String url, String token, String jsonData) throws IOException {
        RequestBody body = RequestBody.create(JSON, jsonData);

        String authToken = String.format("Bearer %s", token);

        Request request = new Request.Builder()
                .header(AUTHORIZATION, authToken)
                .post(body)
                .url(url)
                .build();

        return client.newCall(request).execute();
    }

    public Response getRequest(String url, String token) throws IOException {
        String authToken = String.format("Bearer %s", token);

        Request request = new Request.Builder()
                .header(AUTHORIZATION, authToken)
                .get()
                .url(url)
                .build();

        return client.newCall(request).execute();
    }
}
