package okb.inventory.models.apiclient.authentication;

class AuthResponse {
    private String message;
    private int code;
    private Exception exception;
    private AccountInfo accountInfo;

    public AuthResponse(AccountInfo accountInfo, int code) {
        this.accountInfo = accountInfo;
        this.message = message;
        this.code = code;
        exception = null;
    }

    public AuthResponse(String message, int code) {
        this.message = message;
        this.code = code;
        exception = null;
    }

    public AuthResponse(Exception exception) {
        this.exception = exception;
    }

    public AccountInfo getAccountInfo() {
        return accountInfo;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public Exception getException() {
        return exception;
    }
}
