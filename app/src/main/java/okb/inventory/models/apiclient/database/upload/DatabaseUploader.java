package okb.inventory.models.apiclient.database.upload;

import okb.inventory.models.entities.ResponsiblePerson;

public class DatabaseUploader {
    UploadBaseTask task;

    public void upload(String host, String token, ResponsiblePerson person, UploadBaseCallback callback) {
        task = new UploadBaseTask(host, token, person, callback);
        task.execute();
    }

    public void cancel() {
        if(task == null) return;

        if(!task.isCancelled())
            task.cancel(true);
    }
}
