package okb.inventory.models.apiclient.database.download;

import okb.inventory.models.entities.Housing;

public interface DownloadHousingsCallback {
    void onComplete(Housing[] housings);
    void onFail(int code, String message);
    void onFailConnectionServer(String message);
}
