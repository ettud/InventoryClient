package okb.inventory.models.apiclient.database.download;

public interface DownloadNamesCallback {
    void onComplete(String[] names);
    void onFail(int code, String message);
    void onFailConnectionServer(String message);
}
