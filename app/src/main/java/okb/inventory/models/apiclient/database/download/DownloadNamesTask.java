package okb.inventory.models.apiclient.database.download;

import android.os.AsyncTask;

import com.google.gson.Gson;

import okb.inventory.models.apiclient.ApiClient;
import okhttp3.Response;

class DownloadNamesTask extends AsyncTask<Void, Void, DownloadNamesTask.ApiResponse> {

    private static final String ACTION_URL = "/api/base/names/load";

    private DownloadNamesCallback callback;
    private String            host;
    private ApiClient         client;
    private Gson              gson;
    private String            token;
    private String            username;

    public DownloadNamesTask(String host, String token, String username, DownloadNamesCallback callback) {
        this.host     = host;
        this.token    = token;
        this.callback = callback;
        this.username = username;

        gson   = new Gson();
        client = new ApiClient();
    }

    @Override
    protected ApiResponse doInBackground(Void... voids) {

        String url = host + ACTION_URL;
        try {
            String jsonData = gson.toJson(username);
            Response response = client.postRequest(url, token, jsonData);
            if(response.code() == ApiClient.OK_CODE) {
                String jsonResponse = response.body().string();
                String[] names = gson.fromJson(jsonResponse, String[].class);
                return new ApiResponse(names);
            }
            else {
                return new ApiResponse(response.message());
            }
        }
        catch (Exception e) {
            return new ApiResponse(e.getMessage());
        }
    }

    @Override
    protected void onPostExecute(ApiResponse response) {
        if(isCancelled()) return;

        switch (response.requestCode) {
            case ApiClient.OK_CODE:
                callback.onComplete(response.names);
                break;

            case -1:
                callback.onFailConnectionServer(response.error);
                break;

                default:
                    callback.onFail(response.requestCode, response.error);
                    break;
        }
    }

    class ApiResponse {
        String[] names;
        String   error;
        int      requestCode;

        ApiResponse(String[] names) {
            this.requestCode = ApiClient.OK_CODE;
            this.names = names;
        }

        ApiResponse(int requestCode, String error) {
            this.requestCode = requestCode;
            this.error = error;
        }

        ApiResponse(String error) {
            this.error = error;
            requestCode = -1;
        }
    }
}
