package okb.inventory.models.apiclient.database.upload;

class UploadBaseResponse {
    private Exception exception;
    private String message;
    private int code;

    public UploadBaseResponse(Exception e) {
        this.exception = e;
    }

    public UploadBaseResponse(String message, int code) {
        this.message = message;
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }

    public Exception getException() {
        return exception;
    }
}
