package okb.inventory.models.apiclient.database.download;

import okb.inventory.models.entities.ResponsiblePerson;

public interface DownloadDatabaseCallback {
    void onComplete(ResponsiblePerson responsiblePerson);
    void onFail();
}
