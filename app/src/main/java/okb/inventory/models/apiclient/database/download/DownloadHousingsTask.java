package okb.inventory.models.apiclient.database.download;

import android.os.AsyncTask;

import com.google.gson.Gson;

import okb.inventory.models.apiclient.ApiClient;
import okb.inventory.models.entities.Housing;
import okhttp3.Response;

class DownloadHousingsTask extends AsyncTask<Void, Void, LoadHousingResponse> {

    private static final String ACTION_LOAD_HOUSINGS = "/api/housings/list";

    private String host;
    private String token;
    private ApiClient client;
    private DownloadHousingsCallback callback;

    public DownloadHousingsTask(String host, String token, DownloadHousingsCallback callback) {
        this.host = host;
        this.token = token;
        this.client = new ApiClient();
        this.callback = callback;
    }

    @Override
    protected LoadHousingResponse doInBackground(Void... voids) {
        try {
            String url = host + ACTION_LOAD_HOUSINGS;
            Response response = client.getRequest(url, token);

            LoadHousingResponse responseTask = null;
            if(response.code() == ApiClient.OK_CODE) {
                String jsonResponse = response.body().string();
                Housing[] housings = new Gson().fromJson(jsonResponse, Housing[].class);
                responseTask = new LoadHousingResponse(housings);
            }
            else {
                responseTask = new LoadHousingResponse(response.code(), response.message());
            }

            return responseTask;
        } catch (Exception e) {
            return new LoadHousingResponse(e.getMessage());
        }
    }

    @Override
    protected void onPostExecute(LoadHousingResponse response) {
        if(response.isException()) {
            callback.onFailConnectionServer(response.getError());
            return;
        }
        if (response.getCode() == ApiClient.OK_CODE)
            callback.onComplete(response.getHousings());
        else
            callback.onFail(response.getCode(), response.getError());
    }
}
