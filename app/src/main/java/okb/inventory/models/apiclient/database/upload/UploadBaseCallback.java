package okb.inventory.models.apiclient.database.upload;

public interface UploadBaseCallback {
    void onComplete();
    void onFail(int code, String message);
    void onFailRequest(String message);
}
