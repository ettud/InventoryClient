package okb.inventory.models.apiclient.database.download;

public class LoaderHousings {

    private String host;
    private String token;
    DownloadHousingsTask task;

    public LoaderHousings(String host, String token) {
        this.host = host;
        this.token = token;
    }

    public void load(DownloadHousingsCallback callback) {
        task = new DownloadHousingsTask(host, token, callback);
        task.execute();
    }

    public void cancel() {
        if(task != null && !task.isCancelled())
            task.cancel(true);
    }
}
