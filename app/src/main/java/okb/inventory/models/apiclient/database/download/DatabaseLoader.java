package okb.inventory.models.apiclient.database.download;

public class DatabaseLoader {

    private String host, token;
    DatabaseDownloadTask task;

    public DatabaseLoader(String host, String token) {
        this.host = host;
        this.token = token;
    }

    public void load(String name, DownloadDatabaseCallback callback) {
        task = new DatabaseDownloadTask(host, token, name, callback);
        task.execute();
    }

    public void cancel() {
        if(task != null || !task.isCancelled())
            task.cancel(true);
    }
}
