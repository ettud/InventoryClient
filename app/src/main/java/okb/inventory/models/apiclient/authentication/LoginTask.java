package okb.inventory.models.apiclient.authentication;

import android.os.AsyncTask;

import com.google.gson.Gson;

import okb.inventory.models.apiclient.ApiClient;
import okhttp3.Response;

class LoginTask extends AsyncTask <Void, Void, AuthResponse> {

    private LoginModel model;
    private ApiClient client;
    private LoginCallback callback;
    private String host;
    private Response response;
    
    private static final String ACTION_LOGIN = "/api/account/login";

    public LoginTask(String host, String email, String password, LoginCallback callback) {
        this.host = host;
        model = new LoginModel(email, password);
        client = new ApiClient();
        this.callback = callback;
    }

    @Override
    protected AuthResponse doInBackground(Void... voids) {
        Gson gson = new Gson();
        String jsonData = gson.toJson(model);
        try {
            String url = host + ACTION_LOGIN;
            response = client.postRequest(url, jsonData);
            if(response.code() == ApiClient.OK_CODE) {
                String jsonResponse = response.body().string();
                AccountInfo accountInfo = gson.fromJson(jsonResponse, AccountInfo.class);

                return new AuthResponse(accountInfo, response.code());
            } else {
                return new AuthResponse(response.message(), response.code());
            }
         } catch (Exception e) {
            return new AuthResponse(e);
        }
    }

    @Override
    protected void onPostExecute(AuthResponse authResponse) {
        if(authResponse.getException() != null) {
            callback.onFailRequest(authResponse.getException().getMessage());
            return;
        }

        if(authResponse.getCode() == ApiClient.OK_CODE) {
            callback.onComplete(authResponse.getAccountInfo().getUsername(),
                    authResponse.getAccountInfo().getAccessToken());
        }
        else {
            callback.onFail(authResponse.getCode(), authResponse.getMessage());
        }
    }

    private class LoginModel {
        private String login;
        private String password;

        public LoginModel(String login, String password) {
            this.login = login;
            this.password = password;
        }
    }
}
