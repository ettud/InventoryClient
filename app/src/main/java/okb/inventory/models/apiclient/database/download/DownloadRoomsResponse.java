package okb.inventory.models.apiclient.database.download;

import okb.inventory.models.apiclient.ApiClient;
import okb.inventory.models.entities.Room;

class DownloadRoomsResponse {
    private boolean isException;
    private int code;
    private String error;
    private Room[] rooms;

    public DownloadRoomsResponse(Room[] rooms) {
        this.rooms = rooms;
        this.code = ApiClient.OK_CODE;
        this.error = null;
        this.isException = false;
    }

    public DownloadRoomsResponse(int code, String error) {
        this.code = code;
        this.error = error;
        this.rooms = null;
        this.isException = false;
    }

    public DownloadRoomsResponse(String error) {
        this.error = error;
        this.isException = true;
        this.rooms = null;
        this.code = -1;
    }

    public Room[] getRooms() {
        return rooms;
    }

    public String getError() {
        return error;
    }

    public int getCode() {
        return code;
    }

    public boolean isException() {
        return isException;
    }
}
