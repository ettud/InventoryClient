package okb.inventory.models.apiclient.authentication;

public class Authenticator {
    public static void Authenticate(String host, String login, String password, LoginCallback callback) {
        LoginTask task = new LoginTask(host, login, password, callback);
        task.execute();
    }
}
