package okb.inventory.models.apiclient.database.upload;

import android.os.AsyncTask;

import com.google.gson.Gson;

import java.io.IOException;

import okb.inventory.models.apiclient.ApiClient;
import okb.inventory.models.entities.ResponsiblePerson;
import okhttp3.Response;

class UploadBaseTask extends AsyncTask<Void, Void, UploadBaseResponse> {

    private final String ACTION_UPLOAD_DB   = "/api/base/upload";

    private ResponsiblePerson responsiblePerson;
    private ApiClient client;
    private UploadBaseCallback callback;
    private Gson gson;
    private String token;
    private String host;

    public UploadBaseTask(String host, String token,
                          ResponsiblePerson person, UploadBaseCallback callback) {
        this.token = token;
        this.host = host;
        this.responsiblePerson = person;
        this.callback = callback;
        client = new ApiClient();
        gson = new Gson();
    }

    @Override
    protected UploadBaseResponse doInBackground(Void... voids) {
        String jsonData = gson.toJson(responsiblePerson);
        try {
            String url = host + ACTION_UPLOAD_DB;
            Response response = client.postRequest(url, token, jsonData);
            return new UploadBaseResponse(response.message(), response.code());
        } catch (IOException e) {
            return new UploadBaseResponse(e);
        }
    }

    @Override
    protected void onPostExecute(UploadBaseResponse response) {
        if(isCancelled()) return;

        if(response.getException() != null) {
            callback.onFailRequest(response.getException().getMessage());
            return;
        }

        if(response.getCode() == ApiClient.OK_CODE) {
            callback.onComplete();
        }
        else {
            callback.onFail(response.getCode(), response.getMessage());
        }
    }
}
