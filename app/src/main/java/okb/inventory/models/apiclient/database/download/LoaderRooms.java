package okb.inventory.models.apiclient.database.download;

public class LoaderRooms {

    private String host;
    private String token;
    DownloadRommsTask task;

    public LoaderRooms(String host, String token) {
        this.host = host;
        this.token = token;
    }

    public void load(String titleHousing, DownloadRoomsCallback callback) {
        task = new DownloadRommsTask(host, token, titleHousing, callback);
        task.execute();
    }

    public void cancel() {
        if(task != null && !task.isCancelled())
            task.cancel(true);
    }
}
