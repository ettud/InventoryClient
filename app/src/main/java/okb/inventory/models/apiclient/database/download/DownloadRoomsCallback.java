package okb.inventory.models.apiclient.database.download;

import okb.inventory.models.entities.Room;

public interface DownloadRoomsCallback {
    void onComplete(Room[] rooms);
    void onFail(int code, String error);
    void onFailConnectedServer(String error);
}
