package okb.inventory.models.apiclient.authentication;

class AccountInfo {
    private String username;
    private String accessToken;

    public String getAccessToken() {
        return accessToken;
    }

    public String getUsername() {
        return username;
    }
}
