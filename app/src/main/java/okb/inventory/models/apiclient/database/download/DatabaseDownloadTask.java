package okb.inventory.models.apiclient.database.download;

import android.os.AsyncTask;

import com.google.gson.Gson;

import java.io.IOException;

import okb.inventory.models.apiclient.ApiClient;
import okb.inventory.models.entities.ResponsiblePerson;
import okhttp3.Response;

class DatabaseDownloadTask extends AsyncTask<Void, Void, ResponsiblePerson> {

    private final String ACTION_LOAD_DB = "/api/base/load";

    private String host;
    private String name;
    private DownloadDatabaseCallback callback;
    private ApiClient client;
    private Gson gson;
    private String token;

    public DatabaseDownloadTask(String host, String token, String name, DownloadDatabaseCallback callback) {
        this.host     = host;
        this.token    = token;
        this.name     = name;
        this.callback = callback;
        client        = new ApiClient();
        gson          = new Gson();
    }

    @Override
    protected ResponsiblePerson doInBackground(Void... voids) {
        String jsonData = gson.toJson(name);
        String url = host + ACTION_LOAD_DB;
        try {
            Response response   = client.postRequest(url, token, jsonData);
            String responseData = response.body().string();
            return gson.fromJson(responseData, ResponsiblePerson.class);
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    protected void onPostExecute(ResponsiblePerson responsiblePerson) {
        if(isCancelled()) return;

        if(responsiblePerson != null)
            callback.onComplete(responsiblePerson);
        else
            callback.onFail();
    }
}
