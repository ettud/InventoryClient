package okb.inventory.models.apiclient.authentication;

public interface LoginCallback {
    void onComplete(String username, String token);
    void onFail(int code, String message);
    void onFailRequest(String message);
}