package okb.inventory.models.apiclient.database.download;

import okb.inventory.models.apiclient.ApiClient;
import okb.inventory.models.entities.Housing;

class LoadHousingResponse {
    private boolean isException;
    private String error;
    private int code;
    private Housing[] housings;

    public LoadHousingResponse(Housing[] housings) {
        this.housings = housings;
        code = ApiClient.OK_CODE;
        error = null;
        isException = false;
    }

    public LoadHousingResponse(int code, String error) {
        this.code = code;
        this.error = error;
        this.housings = null;
        this.isException = false;
    }

    public LoadHousingResponse(String error) {
        this.isException = true;
        this.error = error;
        this.code = -1;
        this.housings = null;
    }

    public Housing[] getHousings() {
        return housings;
    }

    public int getCode() {
        return code;
    }

    public String getError() {
        return error;
    }

    public boolean isException() {
        return isException;
    }
}
