package okb.inventory.models.apiclient.database.download;

import android.os.AsyncTask;

import com.google.gson.Gson;

import okb.inventory.models.apiclient.ApiClient;
import okb.inventory.models.entities.Room;
import okhttp3.Response;

class DownloadRommsTask extends AsyncTask<Void,Void, DownloadRoomsResponse> {

    private static final String ACTION_LOAD_ROOMS = "/api/housings/rooms/list";

    private String titleHousing;
    private String host;
    private String token;
    private DownloadRoomsCallback callback;
    private ApiClient client;
    private Gson gson;

    public DownloadRommsTask(String host, String token, String titleHousing,
                             DownloadRoomsCallback callback) {



        this.host = host;
        this.token = token;
        this.titleHousing = titleHousing;
        this.callback = callback;
        client = new ApiClient();
        gson = new Gson();
    }

    @Override
    protected DownloadRoomsResponse doInBackground(Void... voids) {
        try {
            String url = host + ACTION_LOAD_ROOMS;
            String jsonData = gson.toJson(titleHousing);

            Response response = client.postRequest(url, token, jsonData);

            DownloadRoomsResponse responseTask;
            if(response.code() == ApiClient.OK_CODE) {
                String jsonResponse = response.body().string();
                Room[] rooms = gson.fromJson(jsonResponse, Room[].class);
                responseTask = new DownloadRoomsResponse(rooms);
            }
            else responseTask = new DownloadRoomsResponse(response.code(), response.message());

            return responseTask;
        }catch (Exception e) {
            return new DownloadRoomsResponse(e.getMessage());
        }
    }

    @Override
    protected void onPostExecute(DownloadRoomsResponse downloadRoomsResponse) {
        if(downloadRoomsResponse.isException()) {
            callback.onFailConnectedServer(downloadRoomsResponse.getError());
            return;
        }

        if(downloadRoomsResponse.getCode() == ApiClient.OK_CODE) {
            callback.onComplete(downloadRoomsResponse.getRooms());
        }
        else callback.onFail(downloadRoomsResponse.getCode(), downloadRoomsResponse.getError());
    }
}
