package okb.inventory.models.database;

import android.os.AsyncTask;

import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.List;

import okb.inventory.models.entities.Device;
import okb.inventory.models.entities.Inventory;
import okb.inventory.models.entities.ResponsiblePerson;
import okb.inventory.models.entities.Room;

public class UpdaterPerson {

    Task task;

    public void update(ResponsiblePerson updatedPerson, ResponsiblePerson person, UpdatePersonCallback callback) {
        task = new Task(updatedPerson, person, callback);
        task.execute();
    }

    public void cancel() {
        if(task == null) return;

        if(task.isCancelled() == false) {
            task.cancel(true);
        }
    }

    class Task extends AsyncTask<Void, Void, Boolean> {

        private ResponsiblePerson newPerson;
        private ResponsiblePerson updatedPerson;
        private UpdatePersonCallback callback;

        Task(ResponsiblePerson updatedPerson, ResponsiblePerson newPerson, UpdatePersonCallback callback) {
            this.updatedPerson = updatedPerson;
            this.newPerson = newPerson;
            this.callback = callback;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                Inventory foundInventory = updatedPerson.getInventory();
                if(foundInventory != null) {
                    foundInventory.setDateLoad(newPerson.getInventory().getDateLoad());
                    foundInventory.save();

                    List<Device> foundDevices = updatedPerson
                            .getInventory()
                            .getDevices();
                    for (Device device :
                            foundDevices) {
                        device.delete();
                    }

                    List<Device> newDevices = newPerson.getInventory().getDevices();
                    for (Device device :
                            newDevices) {

                        if(device.getRoom() != null) {
                            Room room = Select
                                    .from(Room.class)
                                    .where(Condition.prop("number").eq(device.getRoom().getNumber()))
                                    .first();

                            if (room != null)
                                device.setRoom(room);
                        }

                        device.setInventory(foundInventory);
                    }

                    Device.saveInTx(newDevices);
                }
                else  {
                    Inventory newInventory = newPerson.getInventory();
                    newInventory.setPerson(newPerson);
                    newInventory.save();

                    List<Device> newDevices = newPerson.getInventory().getDevices();
                    for (Device device :
                            newDevices) {
                        device.setInventory(newInventory);
                    }

                    Device.saveInTx(newDevices);
                }

                return true;
            }catch (Exception e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean isSuccess) {
            if(isSuccess)
                callback.onComplete();
            else
                callback.onFail();
        }
    }
}
