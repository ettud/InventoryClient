package okb.inventory.models.database;

public interface LoadNamesCallback {
    void onComplete(String[] names);
    void onFail();
}
