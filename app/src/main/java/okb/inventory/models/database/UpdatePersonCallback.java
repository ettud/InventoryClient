package okb.inventory.models.database;

public interface UpdatePersonCallback {
    void onComplete();
    void onFail();
}
