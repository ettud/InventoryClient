package okb.inventory.models.database;

import okb.inventory.models.entities.ResponsiblePerson;

public interface FoundPersonCallback {
    void onFound(ResponsiblePerson person);
    void onFail();
}
