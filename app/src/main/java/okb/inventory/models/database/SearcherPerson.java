package okb.inventory.models.database;

import android.os.AsyncTask;

import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.List;

import okb.inventory.models.entities.Device;
import okb.inventory.models.entities.Inventory;
import okb.inventory.models.entities.ResponsiblePerson;

public class SearcherPerson {

    private Task task;

    public void search(String fullName, FoundPersonCallback callback) {
        task = new Task(fullName, callback);
        task.execute();
    }

    public void cancel() {
        if(task != null && !task.isCancelled())
            task.cancel(true);
    }

    class Task extends AsyncTask<Void, Void, ResponsiblePerson> {

        private String fullName;
        private FoundPersonCallback callback;

        Task(String fullName, FoundPersonCallback callback) {
            this.fullName = fullName;
            this.callback = callback;
        }

        @Override
        protected ResponsiblePerson doInBackground(Void... voids) {
            String[] fullName = this.fullName.split(" ");

            ResponsiblePerson foundPerson = Select
                    .from(ResponsiblePerson.class)
                    .where(Condition.prop("surname").eq(fullName[0]),
                            Condition.prop("firstname").eq(fullName[1]),
                            Condition.prop("patronymic").eq(fullName[2])).first();

            if(foundPerson == null) return null;

            Inventory inventory = Select
                    .from(Inventory.class)
                    .where(Condition.prop("person").eq(String.valueOf(foundPerson.getId())))
                    .first();

            if(inventory == null) {
                foundPerson.setInventory(null);
                return foundPerson;
            }

            List<Device> devices = Select
                    .from(Device.class)
                    .where(Condition.prop("inventory").eq(String.valueOf(inventory.getId())))
                    .list();

            inventory.setDevices(devices);
            foundPerson.setInventory(inventory);

            return foundPerson;
        }

        @Override
        protected void onPostExecute(ResponsiblePerson person) {
            if(isCancelled()) return;

            if(person != null)
                callback.onFound(person);
            else
                callback.onFail();
        }
    }
}
