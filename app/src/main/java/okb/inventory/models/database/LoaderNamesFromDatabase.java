package okb.inventory.models.database;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import okb.inventory.models.entities.ResponsiblePerson;

public class LoaderNamesFromDatabase {

    Task task;

    public void load(LoadNamesCallback callback) {
        task = new Task(callback);
        task.execute();
    }

    public void cancel() {
        if(task == null) return;

        if(task.isCancelled() == false) {
            task.cancel(true);
        }
    }
    
    class Task extends AsyncTask<Void, Void, String[]> {

        private LoadNamesCallback callback;
        
        Task(LoadNamesCallback callback) {
            this.callback = callback;
        }
        
        @Override
        protected String[] doInBackground(Void... voids) {
            try {
                Iterator<ResponsiblePerson> personsIterator
                        = ResponsiblePerson.findAll(ResponsiblePerson.class);

                if(personsIterator == null) return null;

                List<String> namesPersons = new ArrayList<>();
                while(personsIterator.hasNext()) {
                    ResponsiblePerson person = personsIterator.next();
                    namesPersons.add(String.format("%s %s %s", person.getSurname(), person.getFirstname(),
                            person.getPatronymic()));
                }

                String[] names = new String[namesPersons.size()];
                namesPersons.toArray(names);
                
                return names;
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String[] names) {
            if(names != null)
                callback.onComplete(names);
            else 
                callback.onFail();
        }
    } 
}
