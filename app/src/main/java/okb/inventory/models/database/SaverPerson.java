package okb.inventory.models.database;

import android.os.AsyncTask;

import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.List;

import okb.inventory.models.entities.Device;
import okb.inventory.models.entities.Housing;
import okb.inventory.models.entities.Inventory;
import okb.inventory.models.entities.ResponsiblePerson;
import okb.inventory.models.entities.Room;

public class SaverPerson {

    public void save(ResponsiblePerson savedPerson, SavePersonCallback callback) {
        Task task = new Task(savedPerson, callback);
        task.execute();
    }

    class Task extends AsyncTask<Void, Void, Boolean> {

        private ResponsiblePerson savedPerson;
        private SavePersonCallback callback;

        Task(ResponsiblePerson savedPerson, SavePersonCallback callback) {
            this.savedPerson = savedPerson;
            this.callback = callback;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                Inventory newInventory = savedPerson.getInventory();
                newInventory.setPerson(savedPerson);

                List<Device> newDevices = newInventory.getDevices();
                for (Device device :
                        newDevices) {

                    if(device.getRoom() != null) {
                        Room room = Select
                                .from(Room.class)
                                .where(Condition.prop("number").eq(device.getRoom().getNumber()))
                                .first();

                        if (room != null)
                            device.setRoom(room);
                        else {
                            Housing housing = (Housing) Select
                                    .from(Housing.class)
                                    .where(Condition.prop("title").eq(device.getRoom()
                                            .getHousing()
                                            .getTitle()))
                                    .first();

                            if(housing == null)
                                device.getRoom().getHousing().save();
                            else {
                                device.getRoom().setHousing(housing);
                            }

                            device.getRoom().save();
                        }
                    }
                    device.setInventory(newInventory);
                }

                savedPerson.save();
                newInventory.save();
                Device.saveInTx(newDevices);

                return true;
            }catch (Exception e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean isSuccess) {
            if(isSuccess)
                callback.onComplete();
            else
                callback.onFail();
        }
    }
}
