package okb.inventory.models.database;

public interface SavePersonCallback {
    void onComplete();
    void onFail();
}
